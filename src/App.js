import React, { useCallback, useState } from "react";
import './App.css';
import { Graph } from "react-d3-graph";

function App() {
  const data = {
    nodes: [{ id: "You", fx: 80, fy:650, size: 400, color: "#FFFF00"}, 
    { id: "Core Java" , fx: 250, fy:650, color: "#FFFF00"},
    { id: "Java JEE" , fx: 350, fy:650, color: "#FFFF00"},
    { id: "Level 1" , fx: 450, fy:650, size: 400, color: "#FFA500"},
    { id: "node JS" , fx: 550, fy:650, color: "#FFFF00"},
    { id: "HTML5" , fx: 650, fy:650, color: "#FFFF00"},
    { id: "Level 2" , fx: 650, fy:450 ,labelPosition: "top", size: 300, color: "#FFA500"},
    { id: "Front End Developer" , fx: 550, fy:450, color: "#FFFF00"},
    { id: "Back End Developer" , fx: 550, fy:150,labelPosition: "top"},
    { id: "SQL" , fx: 850, fy:450},
    { id: "Hadoop Lead" , fx: 850, fy:150,labelPosition: "top"},
    { id: "MySQL" , fx: 1050, fy:450},
    { id: "Majento Lead" , fx: 1050, fy:150,labelPosition: "top"},
    { id: "Lead Developer" , fx: 1250, fy:450},
    { id: "SME" , fx: 1250, fy:150,labelPosition: "top"},
    { id: "" , fx: 1050, fy:250},
    { id: " " , fx: 1250, fy:250},
    { id: "  " , fx: 850, fy:250},
    { id: "   " , fx: 550, fy:250},
    { id: "Level 3" , fx: 450, fy:450,labelPosition: "top", color: "#FFA500"},
    { id: "Tester 1" , fx: 350, fy:450},
    { id: "Tester 2" , fx: 250, fy:450, labelPosition: "left"},
    { id: "Level 4" , fx: 250, fy:250, labelPosition: "right", color: "#FFA500"},
    { id: "Project Manager" , fx: 250, fy:150, labelPosition: "top"},
    { id: "QA" , fx: 200, fy:250, labelPosition: "top"},
    { id: "Program Lead" , fx: 80, fy:250, labelPosition: "top"},
    { id: "MVP" , fx: 80, fy:450, labelPosition: "left"}
  ],
    links: [
        { source: "You", target: "Core Java", color: "#FFFF66"},
        { source: "Core Java", target: "Java JEE", color: "#FFFF66"},
        { source: "Java JEE", target: "Level 1", color: "#FFFF66"},
        { source: "Level 1", target: "node JS", color: "#FFFF66"},
        { source: "Level 1", target: "Level 3", color: "#FFFF66"},
        { source: "node JS", target: "HTML5", color: "#FFFF66"},
        { source: "HTML5", target: "Level 2", color: "#FFFF66"},
        { source: "Level 2", target: "Front End Developer", color: "#FFFF66"},
        { source: "Level 2", target: "SQL", color: "#FFFF66"},
        { source: "SQL", target: "MySQL"},
        { source: "SQL", target: "Hadoop Lead"},
        { source: "MySQL", target: "Majento Lead"},
        { source: "", target: "Majento Lead"},
        { source: " ", target: "SME"},
        { source: "", target: "  "},
        { source: "  ", target: "Hadoop Lead"},
        { source: "   ", target: "Back End Developer"},
        { source: "Front End Developer", target: "Back End Developer"},
        { source: "MySQL", target: "Lead Developer"},
        { source: "Lead Developer", target: "SME"},
        { source: "Front End Developer", target: "Level 3", color: "#FFFF66"},
        { source: "Level 3", target: "Tester 1"},
        { source: "Tester 1", target: "Tester 2"},
        { source: "Tester 2", target: "Level 4"},
        { source: "Level 4", target: "Project Manager"},
        { source: "Level 4", target: "QA"},
        { source: "QA", target: "Program Lead"},
        { source: "Program Lead", target: "MVP"},
        { source: "MVP", target: "You"}
    ],
};

// the graph configuration, you only need to pass down properties
// that you want to override, otherwise default ones will be used
const myConfig = {
    nodeHighlightBehavior: true,
    node: {
        color: "white",
        strokeColor: "white",
        size: 250,
        highlightStrokeColor: "yellow",
        highlightColor: "white",
        highlightFontSize: "20px",
        fontColor: "white",
        fontSize: "20px",
        labelPosition: "bottom"
    },
    link: {
      color: "#d3d3d3",
      strokeWidth : 9.5,
    },
    height: 800,
    width : 1400,
    highlightOpacity : 0.2,
    nodeHighlightBehavior : "true",
    freezeAllDragEvents: "true"
};

  return (
    <div className="App">
      <Graph
    id="graph-id" // id is mandatory, if no id is defined rd3g will throw an error
    data={data}
    config={myConfig} 
    freezeAllDragEvents={true}
/>;
    </div>
  );
}
export default App;